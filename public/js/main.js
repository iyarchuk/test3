$('span.glyphicon').click(function () {
    if($(this).hasClass('glyphicon-plus')) {
        $(this).removeClass('glyphicon-plus').addClass('glyphicon-minus');
    } else {
        $(this).addClass('glyphicon-plus').removeClass('glyphicon-minus');
    }
    $(this).parent().parent().children('ul.tree').toggle(200);
});

<?php

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $this->assets
            ->addCss('css/homepage.css');

        //get input vars
        //current category ID
        $categoryID = $this->dispatcher->getParam('itemID', 'int');
        //item url
        $itemUrl = $this->dispatcher->getParam('itemUrl');
        // get number of current page
        $currentPage = $this->dispatcher->getParam('page', 'int');
        if ($currentPage == '') {
            $currentPage = 1;
        }
        $mainCategory = $this->dispatcher->getParam('mainCategory', 'int');

        //prepare search sql
        $sql = "SELECT p.* FROM Products p";
        if ($categoryID > 0) {
            $sql = "SELECT p.* FROM Products p
                    LEFT JOIN Categories c ON p.category_id=c.id
                    WHERE c.id={$categoryID} OR c.parent_id={$categoryID}";
        } else {
            $categoryID = 0;
        }
        //prepare sql result
        $products = new Phalcon\Mvc\Model\Query($sql, $this->getDI());
        $productsData = $products->execute();

        // init paginator
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $productsData,
                "limit" => 9,
                "page" => $currentPage
            )
        );

        // get paginator
        $page = $paginator->getPaginate();
        //set view variables
        $this->view->setVar('page', $page);
        $this->view->setVar('itemUrl', $itemUrl);
        $this->view->setVar('mainCategory', $mainCategory);
        $this->view->setVar('url', $this->di->get('url'));

        $this->view->setVar('categories', Categories::getTree());
    }

    public function productAction()
    {
        $this->assets
            ->addCss('css/product.css');

        $itemID = $this->dispatcher->getParam('itemID', 'int');
        $product = Products::findFirst($itemID)->toArray();

        //get product category
        $productCategory = Categories::findFirst($product['category_id']);

        //set view variables
        $this->view->setVar('product', $product);
        $this->view->setVar('url', $this->di->get('url'));
        $this->view->setVar('itemUrl', $productCategory->url);
        $this->view->setVar('mainCategory', ($productCategory->parent_id != 0 ? $productCategory->parent_id :
            $productCategory->id));
        $this->view->setVar('categories', Categories::getTree());
    }

    public function action404Action()
    {
        $this->assets
            ->addCss('css/homepage.css');

        $this->view->setVar('url', $this->di->get('url'));
        $this->view->setVar('itemUrl', '');
        $this->view->setVar('mainCategory', 0);
        $this->view->setVar('categories', Categories::getTree());
    }

    public function searchAction()
    {
        //set view variables
        $item = $this->dispatcher->getParam('item');
        $page = $this->dispatcher->getParam('page');

        //check category
        $category = Categories::findFirst(array(
            "conditions" => "url = :item:",
            "bind" => array('item' => $item)
        ));
        if (!$category) {
            //check product
            $product = Products::findFirst(array(
                "conditions" => "url = :item:",
                "bind" => array('item' => $item)
            ));

            //if product exist - go to product page
            if (!$product) {
                // 404 page
                $this->dispatcher->forward(array(
                    "action" => "action404",
                ));
            } else {
                $this->dispatcher->forward(array(
                    "action" => "product",
                    "params" => array(
                        'itemID' => $product->id,
                        'itemUrl' => $item,
                    )
                ));
            }
        } else {
            //if category exist - go to category page
            $this->dispatcher->forward(array(
                "action" => "index",
                "params" => array(
                    'itemID' => $category->id,
                    'itemUrl' => $item,
                    'mainCategory' => $category->parent_id,
                    'page' => $page,
                )
            ));
        }
    }
}


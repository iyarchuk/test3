<?php

$router = new \Phalcon\Mvc\Router();

$router->add('/p/{page:[\d]+}', array(
    'controller' => 'index',
    'action' => 'index',
))->setName('paginator');

$router->add('/{item:[\w\d\-_]+}', array(
    'controller' => 'index',
    'action' => 'search',
))->setName('search');

$router->add('/{item:[\w\d\-_]+}/p/{page:[\d]+}', array(
    'controller' => 'index',
    'action' => 'search',
))->setName('category');

$route = $router->getMatchedRoute();

return $router;
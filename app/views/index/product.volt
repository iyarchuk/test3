<div class="thumbnail">
    <img class="img-responsive" src="/img/items/<?=$product['id']?>.jpg" alt="">
    <div class="caption-full">
        <h4 class="pull-right">$<?=number_format($product['price'], 2)?></h4>
        <h4><a href="#"><?=$product['name']?></a></h4>

        <p><?=$product['description']?></p>
    </div>
</div>
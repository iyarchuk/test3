<div class="row text-center">
    <?php $this->partial("partial/paginator") ?>
</div>
<div class="row">
    <?
    if(count($page->items) > 0) {
        foreach ($page->items as $item) {
    ?>
    <div class="col-sm-4 col-lg-4 col-md-4">
        <div class="thumbnail">
            <img src="/img/items/<?=$item->id?>.jpg" alt="">

            <div class="caption">
                <h4 class="pull-right">$<?
                            echo number_format($item->price, 2);
                    ?></h4>
                <h4><a href="<?
                            echo $url->get(array(
                                              'for' => 'search',
                                              'item' => $item->url
                                            ));
                            ?>"><?=$item->name;?></a></h4>

                <p><?=$item->description;?></p>
            </div>
        </div>
    </div>
    <?
            }
    } else {
    ?>
    <div class="row text-center">
        <h4>No items</h4>
    </div>
    <?
    }
    ?>
</div>
<div class="row text-center">
    <?php $this->partial("partial/paginator") ?>
</div>

<?
$route = 'paginator';
if($itemUrl)
{
    $route = 'category';
}

if ($page->total_pages > 1)
{
    $start = 1;
    $end = $page->total_pages;

    if ($page->total_pages > 5)
    {
        $start = $page->current-2;

        if ($start <= 0 || $page->total_pages <= 5)
        {
            $start = 1;
        }
        $end = $start + 4;

        if ($page->total_pages < $end)
        {
            $end = $page->total_pages;
            if ($end - 4 >= 1)
            {
                $start = $end -4;
            }
        }
    }
?>
<ul class="pagination">
    <li><a href="<?=$url->get(array(
                        'for' => $route,
                        'item' => $itemUrl,
                        'page' => 1,
                    ))?>">&laquo;</a></li>
    <?
            for($pageNumber = $start; $pageNumber <= $end; $pageNumber++)
            {
                echo "<li";
                if($pageNumber==$page->current)
                {
                    echo ' class="active"';
                }
                echo ">";
    ?>
            <a href="<?=$url->get(array(
                        'for' => $route,
                        'item' => $itemUrl,
                        'page' => $pageNumber,
                    ))?>"><?=$pageNumber?></a></li>
    <?
            }
    ?>
    <li><a href="<?=$url->get(array(
                        'for' => $route,
                        'item' => $itemUrl,
                        'page' => $page->last,
                    ))?>">&raquo;</a></li>
</ul>
<?
}
?>
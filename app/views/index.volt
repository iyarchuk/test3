<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Igor Y.">

    <title>Test Task</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <?php $this->assets->outputCss() ?>

    <!-- Main CSS -->
    <link href="/css/main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Test Task</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/">Home</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- Page Content -->
<div class="container">
    <div class="row">

        <div class="col-md-3">
            <div class="well well-lg">
                <ul class="nav ">
                    <?
                    foreach ($categories as $item) {
                        if ($item['parent_id'] != 0) continue;
                        $dispChild = 'none';
                        $glyphicon = 'plus';

                        if ($mainCategory!=0 && $mainCategory==$item['id']
                            || $mainCategory==0 && $item['url']==$itemUrl)
                            {
                                $dispChild = 'block';
                                $glyphicon = 'minus';
                            }
                    ?>
                    <li>
                        <label label-default="" class="tree-toggler nav-header <?=($item['url']==$itemUrl ?
                        'label-active' : '')?>">
                            <span class="glyphicon glyphicon-<?=$glyphicon?>"></span> <a href="<?
                            echo $url->get(array(
                                              'for' => 'search',
                                              'item' => $item['url']
                                            ));
                            ?>"><?=$item['name']?></a>
                        </label>
                        <ul class="nav tree active-trial" style="display: <?=$dispChild?>;">
                            <?
                        foreach ($categories as $subItem) {

                            if($subItem['parent_id'] != $item['id']) continue;

                        ?>
                            <li class="<?=($subItem['url']==$itemUrl ? 'li-active' : '')?>"><a href="<?
                            echo $url->get(array(
                                              'for' => 'search',
                                              'item' => $subItem['url']
                                            ));
                            ?>"><?=$subItem['name']?></a></li>
                            <?
                        }
                        ?>
                        </ul>
                    </li>
                    <li class="nav-divider"></li>
                    <?
                    }
                    ?>
                </ul>
            </div>
        </div>

        <div class="col-md-9">
            {{ content() }}
        </div>

    </div>
</div>
<!-- /.container -->

<div class="container">
    <!-- Footer -->
    <footer>
        <div class="row panel-footer text-center navbar-fixed-bottom">
            <div class="col-lg-12">
                <p>&copy; Igor Y. 2014</p>
            </div>
        </div>
    </footer>
</div>
<!-- /.container -->

<!-- jQuery Version 1.11.0 -->
<script src="/js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>
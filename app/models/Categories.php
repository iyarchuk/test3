<?php

class Categories extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $parent_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $url;

    static function getTree()
    {
        $items = Categories::find()
            ->toArray();

        $tree = array();
        foreach ($items as $item) {
            $tree[$item['parent_id']][$item['id']] = $item;
        }
        return $items;
    }
}

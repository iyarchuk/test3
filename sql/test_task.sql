-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Лис 05 2014 р., 18:22
-- Версія сервера: 5.5.24-log
-- Версія PHP: 5.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `test_task`
--

-- --------------------------------------------------------

--
-- Структура таблиці `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  `url` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп даних таблиці `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `url`) VALUES
(1, 0, 'Intuit', '1-950-644-1214'),
(2, 0, 'Roghuis Consultancy', '850-4101'),
(3, 0, 'Open Market', '1-299-4740'),
(4, 0, 'Aerial Survey', '1-843-6329'),
(5, 0, 'PSINet', '1-542-586-7348'),
(6, 4, 'Abena Tech', '1-509-402-6122'),
(7, 3, 'Silicon Graphics', '1-941-196-1719'),
(8, 3, 'KZ Systems', '214-674-4412'),
(9, 1, 'Robot Decora', '126-506-3162'),
(10, 2, 'SwissAero', '1-973-0617'),
(11, 2, 'Fluid Management', '1-532-362-1251'),
(12, 5, 'Vardisio', '1-208-085-8533'),
(13, 2, 'PerfectIT', '1-378-283-3163'),
(14, 5, 'Intuit', '250-5941'),
(15, 4, 'Neerdam Installaties', '1-299-820-7395'),
(16, 1, 'Interbew', '528-574-4558'),
(17, 1, 'Microsoft', '093-9589'),
(18, 1, 'EuroTools', '1-009-960-3745'),
(19, 4, 'GoldStone', '383-997-1524'),
(20, 3, 'WellPoint', '529-223-5851'),
(21, 4, 'Van Meten Woonwinkel', '798-091-3473'),
(22, 3, 'Samsung', '613-767-8787'),
(23, 5, 'CMG Information Services', '484-4198'),
(24, 1, 'Muno', '1-199-429-7669'),
(25, 4, 'ShopCentrale BV', '1-142-586-3102');

-- --------------------------------------------------------

--
-- Структура таблиці `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `price` float NOT NULL,
  `url` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп даних таблиці `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `price`, `url`) VALUES
(1, 2, 'BELLS WEDDING CREPE', 'This is a description 5434609', 59, '574-0925'),
(2, 2, 'Kale', 'This is a description 6388972', 6093, '1-117-2121'),
(3, 2, 'DOOR GREEN LARGE', 'Description number 6309170', 5, '1-069-939-9174'),
(4, 2, 'ELEPHANT GARLIC', 'This is a description 6816458', 9, '562-9966'),
(5, 2, 'SLEEPING BAG BLACK MEDIUM', 'This is a description 2215048', 1113, '295-203-7024'),
(6, 3, 'Leeks', 'This is a description 5621614', 67, '901-3582'),
(7, 3, 'PEAR HALVES', 'This is a description 4520323', 863, '881-7866'),
(8, 3, 'Shark Cartilage', 'This is a description 5094303', 27621, '423-859-4129'),
(9, 3, 'COFFEE FILTER NR2', 'Description number 3931187', 80, '1-203-811-3538'),
(10, 3, 'FEATHER DUSTER OSTRICH', 'Description number 2170747', 6, '331-0986'),
(11, 3, 'Cayenne', 'This is a description 6120811', 82810, '556-823-8957'),
(12, 4, 'BEEF JERKY ORIGINAL', 'This is a description 5300298', 8408, '1-421-263-7358'),
(13, 4, 'Mustard', 'This is a description 1528640', 7590, '1-262-166-4914'),
(14, 4, 'Gourd Seed Corn', 'Description number 3549654', 8577, '353-475-4393'),
(15, 5, 'Herbal Detox Program', 'Description number 8446655', 4889, '1-617-773-8800'),
(16, 5, 'DHEA', 'Description number 2635133', 108, '1-254-724-4254'),
(17, 5, 'CLAMS MINCED GORTON', 'Description number 5914864', 987, '419-280-4107'),
(18, 5, 'Cabbage', 'This is a description 2394310', 2, '983-8760'),
(19, 5, 'Goldenseal', 'Description number 0945896', 25, '1-243-803-1899'),
(20, 5, 'Bee Pollen', 'Description number 2602236', 131, '406-439-3428'),
(21, 5, 'COFFEE FILTER NR2', 'This is a description 4701248', 83, '049-145-5258'),
(22, 6, 'Rutabaga', 'This is a description 6386822', 7, '1-898-541-3373'),
(23, 6, 'LINER HVY 22X14X58 BLACK', 'Description number 0245705', 42, '1-522-878-9416'),
(24, 6, 'SALMON WHOLE', 'Description number 4974706', 4040, '1-755-074-4617'),
(25, 6, 'Emu Magic Eyes', 'Description number 6716027', 43, '368-432-9732'),
(26, 6, 'BEVERAGE CUP', 'Description number 9167456', 7124, '1-238-7264'),
(27, 6, 'Goto Kola', 'Description number 6787615', 3, '844-921-2636'),
(28, 7, 'Rutabaga', 'This is a description 5781268', 8, '1-412-5627'),
(29, 7, 'Citrin', 'Description number 2536338', 4, '1-341-396-9857'),
(30, 8, 'JUICE GRAPEFRUIT', 'This is a description 4307165', 8133, '1-288-045-3422'),
(31, 8, 'COOKING WINE RED BURGUNDY', 'Description number 8924413', 2838, '1-471-851-0019'),
(32, 8, 'Hot Peppers', 'Description number 9192463', 19, '911-210-7308'),
(33, 8, 'Kale', 'This is a description 6614658', 56, '1-539-3775'),
(34, 8, 'Apple Cider Vinegar', 'Description number 5919253', 831, '1-926-4404'),
(35, 9, 'Tea Tree Oil', 'This is a description 4395576', 67539, '131-8787'),
(36, 9, 'Dent Corn', 'This is a description 5047041', 42, '144-338-2236'),
(37, 9, 'Chasteberry Tea', 'This is a description 2392113', 2, '1-095-155-8463'),
(38, 10, 'Shell Peas', 'This is a description 4498882', 9, '1-706-9528'),
(39, 10, 'Spice Basket', 'This is a description 4699003', 1316, '1-368-939-9044'),
(40, 10, 'Peanuts', 'This is a description 7578190', 79306, '183-6382'),
(41, 10, 'Lettuce', 'This is a description 2278005', 2819, '1-746-425-0935'),
(42, 10, 'Dry Blub Onions', 'This is a description 4911748', 3774, '581-589-1678'),
(43, 10, 'Ginger Root', 'This is a description 4719422', 6087, '116-9161'),
(44, 11, 'Emu Massage Cream', 'Description number 7471087', 72631, '258-3141'),
(45, 11, 'Kohlrabi', 'This is a description 7450808', 7726, '1-978-460-2624'),
(46, 11, 'MOUNTAIN DEW PRE-MIX', 'Description number 4279674', 3261, '1-416-042-0003'),
(47, 11, 'Kava Kava', 'This is a description 0839088', 9, '1-583-7940'),
(48, 11, 'Cinnamon Spice', 'Description number 6406156', 1796, '1-632-344-1863'),
(49, 11, 'Goldenseal', 'This is a description 3594554', 75, '495-884-6148'),
(50, 11, 'Emu Massage Cream', 'This is a description 2855775', 51, '1-730-5311'),
(51, 11, 'GATORADE GLACIER ICE', 'This is a description 0063772', 53, '1-032-651-0073'),
(52, 11, 'Ornamental Wheat', 'This is a description 8437530', 18, '218-2562'),
(53, 11, 'Broccoli', 'Description number 8248975', 70183, '1-801-101-8575'),
(54, 12, 'Gingko Plus 4', 'Description number 4262514', 9785, '1-383-746-6666'),
(55, 12, 'HAMBURG DILL CHIPS', 'Description number 1606958', 11, '1-976-2710'),
(56, 12, 'WATER DASANI BTL', 'This is a description 4218987', 7, '669-2311'),
(57, 12, 'Hot Peppers', 'This is a description 4356801', 2, '1-101-3895'),
(58, 12, 'TABLE WHITE SMALL', 'Description number 8233746', 2, '1-280-8002'),
(59, 12, 'RAISIN COOKIE FILLED', 'This is a description 5249163', 7, '1-423-7570'),
(60, 13, 'Japanese Green Tea', 'This is a description 2711142', 7, '1-607-873-7413'),
(61, 13, 'MEZZI RIGATONI', 'Description number 7844820', 5491, '1-905-689-4455'),
(62, 13, 'Goldenseal', 'This is a description 0567154', 45, '297-370-0491'),
(63, 13, 'Beans', 'Description number 4011706', 85422, '020-5937'),
(64, 14, 'Pumpkins', 'This is a description 9251474', 9, '1-476-7824'),
(65, 14, 'TABLE BASE 4 PRONG', 'This is a description 8961540', 183, '1-331-366-2023'),
(66, 14, 'COFFEE FILTER NR2', 'Description number 5837500', 4, '1-088-0263'),
(67, 14, 'Peppermint Oil', 'This is a description 4843919', 239, '1-682-1386'),
(68, 14, 'Evening Primrose Oil', 'Description number 8959450', 2, '1-588-5929'),
(69, 14, 'Echinacea', 'Description number 4663741', 4930, '1-270-8730'),
(70, 14, 'BEANS BLACK', 'Description number 1927069', 6, '210-587-4071'),
(71, 14, 'Cayenne Plus', 'Description number 1711392', 11979, '1-745-2862'),
(72, 14, 'Chasteberry Tea', 'Description number 3234892', 10436, '597-622-6542'),
(73, 14, 'MANDARIN ORANGES', 'This is a description 7606854', 7, '1-395-9441'),
(74, 15, 'SWEETENER NATRA TASTE', 'This is a description 4483218', 7034, '872-498-5991'),
(75, 15, 'Dent Corn', 'This is a description 1240920', 5, '960-923-6606'),
(76, 15, 'HAM HOCKS', 'This is a description 0876306', 14, '852-260-8082'),
(77, 15, 'Ginger Root', 'This is a description 2127090', 21, '1-609-269-0442'),
(78, 15, 'HEALTHY CHOICE PASTA', 'Description number 4624758', 8109, '691-4803'),
(79, 15, 'GARLIC MIST AERO', 'This is a description 0129209', 261, '169-774-6484'),
(80, 16, 'Cabbage', 'Description number 6821403', 48, '279-0051'),
(81, 16, 'FEATHER DUSTER OSTRICH', 'This is a description 0746706', 69, '1-886-0971'),
(82, 16, 'Ginger Root', 'This is a description 6583846', 63, '655-962-9893'),
(83, 16, 'APRICOT FILLED COOKIE', 'This is a description 4084587', 5, '785-6046'),
(84, 16, 'VANILLA BUTTERNUT', 'This is a description 3803197', 795, '1-267-2142'),
(85, 17, 'Bilberry', 'Description number 5818060', 71877, '1-470-4845'),
(86, 17, 'JUICE GRAPEFRUIT', 'Description number 1667604', 4352, '044-9194'),
(87, 17, 'Turnips', 'Description number 4912404', 533, '742-571-5786'),
(88, 18, 'CHIP DISH SHELL', 'Description number 2546239', 629, '1-128-260-0043'),
(89, 18, 'Cinnamon Oil', 'Description number 4802751', 84, '286-579-3151'),
(90, 19, 'Slicing Cucumbers', 'Description number 8973006', 7107, '1-427-060-2986'),
(91, 19, 'BANQUET RL WHITE', 'Description number 3680604', 6286, '1-124-314-7975'),
(92, 19, 'Small Fruited/Cherry Tomatoes', 'Description number 1369009', 5804, '1-798-6275'),
(93, 20, 'Corn', 'This is a description 4697053', 132, '473-682-7764'),
(94, 20, 'BUTCHER PAPER WHITE', 'This is a description 9393319', 3, '948-690-6549'),
(95, 20, 'OLIVES STFD W/ANCHOVIE', 'This is a description 3675654', 23287, '1-533-3678'),
(96, 20, 'BEVNAPS ROYAL BLUE', 'Description number 9583868', 93, '442-4768'),
(97, 20, 'Gourd Seed Corn', 'Description number 0666843', 61, '1-681-4379'),
(98, 21, 'Cauliflower', 'This is a description 7710840', 92034, '1-835-309-6206'),
(99, 21, 'Bi-Color Tomatoes', 'Description number 9298736', 78, '1-684-225-8597'),
(100, 21, 'OLIVE MIST COATING', 'This is a description 4025085', 512, '214-8482');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
